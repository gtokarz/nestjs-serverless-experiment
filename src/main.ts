import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import * as express from 'express';
import { Handler, Context } from 'aws-lambda';
import { Server } from 'http';
import { createServer, proxy } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';

const isLambdaCall = process.env.LAMBDA_TASK_ROOT || process.env.AWS_EXECUTION_ENV || process.env.AWS_SAM_LOCAL;

async function setupApp(app: INestApplication) {
  app.use(eventContext());
}

class ServerlessExpressProvider {
  private static instance: Server;

  public static async getServer() {
    if (!ServerlessExpressProvider.instance) {
      const expressApp = express();
      const nestApp = await NestFactory.create(AppModule, new ExpressAdapter(expressApp));
      nestApp.setGlobalPrefix('hello');
      await setupApp(nestApp);
      await nestApp.init();
      ServerlessExpressProvider.instance = createServer(expressApp, undefined, []);
    }
    return ServerlessExpressProvider.instance;
  }
}

async function bootstrap() {
    const expressApp = express();
    const nestApp = await NestFactory.create(AppModule, new ExpressAdapter(expressApp));
    nestApp.setGlobalPrefix('hello');
    await nestApp.listen(3005);
}

export const handler: Handler = async (event: any, context: Context) => {
  const server = await ServerlessExpressProvider.getServer();
  return proxy(server, event, context, 'PROMISE').promise;
}

if (!isLambdaCall) {
  bootstrap();
}